const { Schema, model, Types } = require("mongoose");

const boardSchema = new Schema({
  label: { type: String },
  title: { type: String },
  description: { type: String },
  cover: { type: String },
  author: { type: Types.ObjectId, required: true, ref: "User" },
  coauthors: [{ type: Types.ObjectId, ref: "User" }],
  links: [{ type: Types.ObjectId, ref: "Link" }],
});

module.exports = model("Board", boardSchema);
