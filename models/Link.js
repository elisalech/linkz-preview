const { Schema, model, Types } = require("mongoose");

const linkSchema = new Schema({
  title: { type: String },
  description: { type: String },
  cover: { type: String },
  link: { type: String },
  board: { type: Types.ObjectId, ref: "Board" },
});

module.exports = model("Link", linkSchema);
