const { Schema, model, Types } = require("mongoose");

const userSchema = new Schema({
  name: { type: String },
  providerInfo: { type: String, required: true },
  provider: { type: String },
  boards: [{ type: Types.ObjectId, ref: "Board" }],
  favorites: [{ type: Types.ObjectId, ref: "Board" }],
});

module.exports = model("User", userSchema);
