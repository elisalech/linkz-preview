const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20");
const config = require("config");
const {
  handleUserAuth,
  handleDeserialize,
} = require("../controllers/user-controllers");

const googleClientID = process.env.GOOGLE_CLIENT_ID;
const googleClientSecret = process.env.GOOGLE_CLIENT_SECRET;

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await handleDeserialize(id);
  done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      callbackURL: `${config.get("baseUrl")}/api/auth/google/redirect`,
      clientID: googleClientID,
      clientSecret: googleClientSecret,
    },
    (accessToken, refreshToken, profile, done) => {
      // console.log("profile", profile);
      handleUserAuth(profile, done);
    }
  )
);
