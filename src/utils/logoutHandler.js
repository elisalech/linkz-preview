import { BASE_URL } from "../consts/config";

export default () => {
  localStorage.removeItem("userInfo");

  window.location.href = `${BASE_URL}/api/auth/logout`;
};
