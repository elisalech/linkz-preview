const labels = {
  private: "Личная",
  common: "Публичная",
  collaborative: "Общий доступ",
};

export default (label) => labels[label];
