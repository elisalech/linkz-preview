import { getLinkPreview } from "link-preview-js";
import ogs from "open-graph-scraper";
import { CORS_PROXY } from "../consts/config";

export default async (query, handler) => {
  const options = {
    url: CORS_PROXY + query,
  };
  ogs(options, (error, { data }) => {
    handler({ error, data });
  });
};
