import getDocHeight from "./getDocHeight";

let lastScrollTop = 0;
const delta = 5;

const docHeight = getDocHeight();

export default (menuH, setToggleCls) => {
  const st = window.scrollY;
  if (Math.abs(lastScrollTop - st) <= delta) return;

  if (st > lastScrollTop && st > menuH) {
    setToggleCls("nav-up");
  } else {
    if (st < docHeight) {
      setToggleCls("nav-down");
    }
  }
  lastScrollTop = st;
};
