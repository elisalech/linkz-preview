export default (boards, filter, uinfo) => {
  switch (filter) {
    case "?query=private":
      return boards.filter((b) => b.label === "private");
    case "?query=common":
      return boards.filter((b) => b.label === "common");
    case "?query=favourited":
      return boards.filter((b) => uinfo.favorites.includes(b._id));
    default:
      return boards;
  }
};
