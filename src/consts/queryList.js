export default [
  { link: "", title: "Все" },
  { link: "private", title: "Личные" },
  { link: "common", title: "Публичные" },
  // { link: "collaborative", title: "Общий доступ" },
  { link: "favourited", title: "Избранное" },
];
