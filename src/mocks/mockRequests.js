import mockLinks from "./mockLinks";
import mockBoards from "./mockBoards";

const promiseResponse = (data) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, 500);
  });

export const getLinks = (id) => promiseResponse(mockLinks);

export const getBoards = (id) => promiseResponse(mockBoards);
