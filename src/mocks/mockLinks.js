export default [
  {
    link: "https://reactjs.org/docs/react-dom.html",
    cover: "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg",
    title: "React DOM",
    description: "A JavaScript library for building user interfaces",
    id: 1,
  },
  {
    link: "http://www.generative-gestaltung.de/2",
    cover: "https://miro.medium.com/max/2880/1*-egs-meZ08WEmAhlwa481Q.png",
    title: "Generative Design",
    description: "Generative Design, Creative Coding on the Web",
    id: 2,
  },
  {
    link: "https://opensourcerover.jpl.nasa.gov/#!/home",
    cover: "https://opensourcerover.jpl.nasa.gov/assets/meta/social.jpg",
    title: "Nasa Rover",
    description:
      "Learn to build robots: Build your very own Mars rover at home!",
    id: 3,
  },
  {
    link: "https://moscowmusicschool.ru",
    cover: "https://moscowmusicschool.ru/assets/og.png",
    title: "Moscow Music School",
    description:
      "Moscow Music School — школа современной музыки. Мы вдохновляем и обучаем тех, кто завтра перевернет музыкальную индустрию в России и мире.",
    id: 4,
  },
  {
    link: "http://www.generative-gestaltung.de/2",
    cover: "https://miro.medium.com/max/2880/1*-egs-meZ08WEmAhlwa481Q.png",
    title: "Generative Design",
    description: "Generative Design, Creative Coding on the Web",
    id: 5,
  },
  {
    link: "https://opensourcerover.jpl.nasa.gov/#!/home",
    cover: "https://opensourcerover.jpl.nasa.gov/assets/meta/social.jpg",
    title: "Nasa Rover",
    description:
      "Learn to build robots: Build your very own Mars rover at home!",
    id: 6,
  },
  {
    link: "https://moscowmusicschool.ru",
    cover: "https://moscowmusicschool.ru/assets/og.png",
    title: "Moscow Music School",
    description:
      "Moscow Music School — школа современной музыки. Мы вдохновляем и обучаем тех, кто завтра перевернет музыкальную индустрию в России и мире.",
    id: 7,
  },
];
