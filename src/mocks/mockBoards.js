export default [
  {
    links: [1, 2, 3],
    cover:
      "https://sun9-35.userapi.com/c851324/v851324986/11690a/LPlEKFjp68o.jpg",
    title: "Плавание",
    description: "",
    label: "Приватная",
  },
  {
    links: [2, 3, 4],
    cover: "https://sun9-71.userapi.com/c7002/v7002414/7dc49/w8NlArsZpVs.jpg",
    title: "Учеба",
    description: "111111",
    label: "Общая",
  },
  {
    links: [6, 7],
    cover:
      "https://sun9-47.userapi.com/c858432/v858432620/b48f9/QQ2mefivcyE.jpg",
    title: "=)",
    description: "22222",
    label: "",
  },
  {
    links: [1, 3, 4],
    cover: "https://sun9-71.userapi.com/c7002/v7002414/7dc49/w8NlArsZpVs.jpg",
    title: "Nonsense",
    description: "3333",
    label: "",
  },
  {
    links: [1, 2, 3, 4],
    cover:
      "https://sun9-28.userapi.com/c857324/v857324670/56b95/TYd6_s8yGpA.jpg",
    title: "Cобаки",
    description: "И все о них",
    label: "",
  },
  {
    links: [4, 5],
    cover:
      "https://sun9-19.userapi.com/c636130/v636130786/403ca/N-0fh4Lu0U8.jpg",
    title: "frogs",
    description: "Лягушки лучшие существа на планете Земля в Солнечной системе",
    label: "",
  },
];
