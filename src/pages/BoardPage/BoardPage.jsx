import React, { useEffect } from "react";

import style from "./BoardPage.module.css";

import LinkItem from "../../components/LinkItem/LinkItem";
import BoardHeader from "../../components/BoardHeader/BoardHeader";
import { connect } from "react-redux";
import requestLinks from "../../requests/requestLinks";
import Loader from "../../UI/Loader/Loader";
import LinkForm from "../../components/forms/LinkForm";
import ErrorPage from "../ErrorPage/ErrorPage";
import ConfirmModal from "../../components/ConfirmModal/ConfirmModal";
import SharePanel from "../../components/SharePanel/SharePanel";

const BoardPage = ({
  match: {
    params: { bid },
  },
  board,
  links,
  getLinks,
  loading,
  toggleLink,
  isError,
  toggleConfirm,
  showShare,
}) => {
  useEffect(() => {
    getLinks(bid);
  }, [bid, getLinks]);
  useEffect(() => {
    if (board) document.title = board.title;
    return () => (document.title = "Linkz");
  }, [board]);
  return (
    <>
      {board && <BoardHeader id={bid} len={links.length} {...board} />}
      {toggleLink && <LinkForm />}
      {toggleConfirm && <ConfirmModal />}
      {showShare && <SharePanel />}
      {loading ? (
        <Loader title="Ссылки грузятся..." />
      ) : (
        <section className={style.links_container}>
          {links &&
            links.map((l, i) => (
              <LinkItem
                key={l._id}
                id={l._id}
                link={l.link}
                cover={l.cover}
                title={l.title}
                description={l.description}
                num={i + 1}
                author={board && board.author}
              />
            ))}
          {isError && <ErrorPage />}
        </section>
      )}
    </>
  );
};

const mapStateToProps = (
  {
    loading: { links: loading },
    togglers: { link, confirm, share },
    boards,
    links,
    errors: { isError },
  },
  {
    match: {
      params: { bid },
    },
  }
) => ({
  loading,
  toggleLink: link,
  toggleConfirm: confirm,
  board: boards.find((b) => b._id === bid),
  showShare: share,
  links,
  isError,
});

const mapDispatchToProps = (dispatch) => ({
  getLinks: (id) => dispatch(requestLinks(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BoardPage);
