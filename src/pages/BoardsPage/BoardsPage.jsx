import React from "react";
import { connect } from "react-redux";

import style from "./BoardsPage.module.css";

import BoardItem from "../../components/BoardItem/BoardItem";
import FilterMenu from "../../components/FilterMenu/FilterMenu";
import BoardForm from "../../components/forms/BoardForm";
import Loader from "../../UI/Loader/Loader";
import boardFilter from "../../utils/boardFilter";
import SharePanel from "../../components/SharePanel/SharePanel";
import ConfirmModal from "../../components/ConfirmModal/ConfirmModal";
import ErrorPage from "../ErrorPage/ErrorPage";

const BoardsPage = ({
  toggleBoard,
  loading,
  boards,
  isLogged,
  showShare,
  toggleConfirm,
}) => {
  return (
    <>
      <FilterMenu isLogged={isLogged} />
      <section className={style.boards_container}>
        {toggleBoard && <BoardForm />}
        {toggleConfirm && <ConfirmModal />}
        {showShare && <SharePanel />}
        {loading ? (
          <Loader title="Борды грузятся..." />
        ) : boards.length > 0 ? (
          boards.map((b) => (
            <BoardItem
              key={b._id}
              links={b.links}
              cover={b.cover}
              title={b.title}
              id={b._id}
              author={b.author}
              description={b.description}
              len={b.links.length}
              label={b.label}
            />
          ))
        ) : (
          <ErrorPage
            noCount={true}
            customMessage={"Кажется, здесь пусто. Создайте новую доску!"}
          />
        )}
      </section>
    </>
  );
};

const mapStateToProps = (
  {
    loading: { boards: loading },
    togglers: { board, share, confirm },
    boards,
    user: { info },
    error,
  },
  { filter }
) => ({
  isLogged: info,
  loading,
  toggleBoard: board,
  toggleConfirm: confirm,
  showShare: share,
  boards: boardFilter(boards, filter, info),
});

export default connect(mapStateToProps, null)(BoardsPage);
