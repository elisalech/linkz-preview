import React from "react";

import style from "./AuthPage.module.css";
import logo from "../../assets/logo.svg";

import { BASE_URL } from "../../consts/config";
import Popup from "../../UI/Popup/Popup";

export default () => {
  return (
    <Popup dark={true}>
      <div className={style.page__container}>
        <img src={logo} alt="" className={style.logo} />
        <p className={style.page_description}>
          Войдите или создайте аккаунт, чтобы начать сохранять закладки со всего
          интернета. Создавайте подборки по категориям и{"\u00A0"}делитесь ими с
          друзьями и коллегами.
        </p>
        <a href={`${BASE_URL}/api/auth/google`}>
          <div className={style.login_btn}>
            <span></span>
            Sign in with Google
          </div>
        </a>
      </div>
    </Popup>
  );
};
