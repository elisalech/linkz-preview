import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import style from "./ErrorPage.module.css";
import { toggleError } from "../../store/errors/actions";
import { useHistory } from "react-router-dom";

const ErrorPage = ({ closePage, errorMessage, customMessage, noCount }) => {
  const [count, setCount] = useState(4);
  const history = useHistory();

  useEffect(() => {
    if (noCount) return;
    let myInterval = setInterval(() => {
      if (count === 1) {
        clearInterval(myInterval);
        setCount(0);
        closePage();
      } else {
        setCount(count - 1);
      }
    }, 1000);
    return () => {
      clearInterval(myInterval);
      if (!count) history.push("/boards");
    };
  });
  return (
    <div className={style.container}>
      <h1 className={style.title}>{customMessage || errorMessage}</h1>
      {!noCount && (
        <p className={style.count}>
          Вы будете перенаправлены к доскам через {count}
        </p>
      )}
    </div>
  );
};

const mapStateToProps = ({ errors: { errorMessage } }) => ({
  errorMessage,
});

const mapDispatchToProps = (dispatch) => ({
  closePage: () => dispatch(toggleError(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorPage);
