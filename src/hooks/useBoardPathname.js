import { useLocation } from "react-router-dom";

export default () => {
  const { pathname } = useLocation();
  const isBoard = pathname.includes("/board/");
  const boardId = isBoard ? pathname.split("/board/")[1] : "";
  return { isBoard, boardId };
};
