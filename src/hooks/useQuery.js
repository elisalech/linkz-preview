import { useLocation } from "react-router-dom";

export default () => {
  return useLocation().search;
};
