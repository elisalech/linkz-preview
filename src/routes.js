import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import Menubar from "./components/Menubar/Menubar";
import MainContainer from "./containers/MainContainer/MainContainer";
import AuthPage from "./pages/AuthPage/AuthPage";

export default (authenticated) => {
  return (
    <Router>
      <Menubar />
      <MainContainer />
    </Router>
  );

  // if (authenticated) {
  //   return (
  //     <Router>
  //       <Menubar />
  //       <MainContainer />
  //     </Router>
  //   );
  // } else if (authenticated === false) {
  //   return (
  //     <Router>
  //       <Route path="/auth" component={AuthPage} />
  //       <Redirect to="/auth" />
  //     </Router>
  //   );
  // } else return null;
};
