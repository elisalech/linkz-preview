import { createAction } from "redux-act";

export const loadingBoards = createAction("Loading boards");
export const loadingLinks = createAction("Loading links");
export const loadingUser = createAction("Loading user");
