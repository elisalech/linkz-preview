import { createReducer } from "redux-act";
import { loadingBoards, loadingLinks, loadingUser } from "./actions";

const initialState = { boards: false, links: false, user: false };

export default createReducer(
  {
    [loadingBoards]: (state, payload) => ({ ...state, boards: payload }),
    [loadingLinks]: (state, payload) => ({ ...state, links: payload }),
    [loadingUser]: (state, payload) => ({ ...state, user: payload }),
  },
  initialState
);
