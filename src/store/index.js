import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";

import user from "./user/";
import loading from "./loading/";
import boards from "./boards/";
import links from "./links";
import togglers from "./togglers";
import errors from "./errors";

const rootReducer = combineReducers({
  user,
  loading,
  boards,
  links,
  togglers,
  errors,
});

let middleware = [thunk];
if (process.env.NODE_ENV !== "production") {
  middleware = [...middleware, createLogger()];
}

const store = createStore(rootReducer, applyMiddleware(...middleware));

export default store;
