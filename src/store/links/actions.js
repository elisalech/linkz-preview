import { createAction } from "redux-act";

export const setLinks = createAction("Set links");
export const addLink = createAction("Add link");
export const removeLink = createAction("Remove link");
export const updateLink = createAction("Update link", (id, newLink) => ({
  id,
  newLink,
}));
