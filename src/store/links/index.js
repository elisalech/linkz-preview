import { createReducer } from "redux-act";
import { setLinks, addLink, removeLink, updateLink } from "./actions";

const initialState = [];

export default createReducer(
  {
    [setLinks]: (state, payload) => [...payload],
    [addLink]: (state, payload) => [payload, ...state],
    [removeLink]: (state, payload) =>
      state.filter((link) => link._id !== payload),
    [updateLink]: (state, { id, newLink }) =>
      state.map((link) => (link.id === id ? newLink : link)),
  },
  initialState
);
