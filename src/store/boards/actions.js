import { createAction } from "redux-act";

export const addBoard = createAction("Add new board");
export const setBoards = createAction("Set boards");
export const removeBoard = createAction("Remove board");
export const updateBoard = createAction("Edit board", (id, newBoard) => ({
  id,
  newBoard,
}));
