import { createReducer } from "redux-act";
import { setBoards, removeBoard, updateBoard, addBoard } from "./actions";

const initialState = [];

export default createReducer(
  {
    [setBoards]: (state, payload) => [...payload],
    [addBoard]: (state, payload) => [payload, ...state],
    [removeBoard]: (state, payload) =>
      state.filter((board) => board._id !== payload),
    [updateBoard]: (state, { id, newBoard }) =>
      state.map((board) => (board._id === id ? newBoard : board)),
  },
  initialState
);
