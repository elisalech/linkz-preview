import { createAction } from "redux-act";

export const toggleNewBoard = createAction("Toggle new board");
export const toggleNewLink = createAction("Toggle new link");
export const toggleUserAction = createAction("Toggle user action");
export const toggleShare = createAction("Toggle share action");
export const setShareAction = createAction("Set share action");
export const toggleConfirmation = createAction("Toggle confirmation");
export const setConfirmationTitle = createAction("Set confirmation title");
export const setConfirmHandler = createAction("Set confirm handler");
