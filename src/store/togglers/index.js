import { createReducer } from "redux-act";
import {
  toggleNewBoard,
  toggleNewLink,
  toggleUserAction,
  toggleShare,
  setShareAction,
  toggleConfirmation,
  setConfirmationTitle,
  setConfirmHandler,
} from "./actions";

const initialState = {
  board: false,
  link: false,
  user: false,
  share: false,
  shareId: null,
  confirm: false,
  confirmTitle: null,
  confirmHandler: null,
};

export default createReducer(
  {
    [toggleNewBoard]: (state, payload) => ({ ...state, board: payload }),
    [toggleNewLink]: (state, payload) => ({ ...state, link: payload }),
    [toggleUserAction]: (state, payload) => ({ ...state, user: payload }),
    [toggleShare]: (state, payload) => ({ ...state, share: payload }),
    [setShareAction]: (state, payload) => ({ ...state, shareId: payload }),
    [toggleConfirmation]: (state, payload) => ({ ...state, confirm: payload }),
    [setConfirmationTitle]: (state, payload) => ({
      ...state,
      confirmTitle: payload,
    }),
    [setConfirmHandler]: (state, payload) => ({
      ...state,
      confirmHandler: payload,
    }),
  },
  initialState
);
