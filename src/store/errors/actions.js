import { createAction } from "redux-act";

export const toggleError = createAction("Toggle new error");
export const setErrorMessage = createAction("Toggle error message");
