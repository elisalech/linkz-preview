import { createReducer } from "redux-act";
import { toggleError, setErrorMessage } from "./actions";

const initialState = { isError: false, errorMessage: "" };

export default createReducer(
  {
    [toggleError]: (state, payload) => ({ ...state, isError: payload }),
    [setErrorMessage]: (state, payload) => ({
      ...state,
      errorMessage: payload,
    }),
  },
  initialState
);
