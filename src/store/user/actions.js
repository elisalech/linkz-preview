import { createAction } from "redux-act";

export const authUserAction = createAction(
  "Auth user",
  (authenticated, info) => ({
    authenticated,
    info,
  })
);
export const resetUser = createAction("Reset user");
