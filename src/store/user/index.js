import { createReducer } from "redux-act";
import { authUserAction, resetUser } from "./actions";

const initialState = {
  info: null,
  authenticated: null,
};

export default createReducer(
  {
    [authUserAction]: (state, { authenticated, info }) => ({
      authenticated,
      info,
    }),
    [resetUser]: (state, payload) => ({ ...state, info: payload }),
  },
  initialState
);
