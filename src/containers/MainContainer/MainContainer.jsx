import React, { useEffect } from "react";
import { Route, Redirect, Switch } from "react-router-dom";

import style from "./MainContainer.module.css";

import BoardPage from "../../pages/BoardPage/BoardPage";
import BoardsPage from "../../pages/BoardsPage/BoardsPage";
import { connect } from "react-redux";
import requestBoards from "../../requests/requestBoards";
import useQuery from "../../hooks/useQuery";
import AuthPage from "../../pages/AuthPage/AuthPage";
import ErrorPage from "../../pages/ErrorPage/ErrorPage";

const MainContainer = ({ requestBoards, boards }) => {
  useEffect(() => {
    if (boards.length > 0) return;
    requestBoards();
  });
  const query = useQuery();
  return (
    <main className={style.main_wrap}>
      <Switch>
        <Route path="/board/:bid" exact={true} component={BoardPage} />
        <Route path="/auth" component={AuthPage} />
        <Route
          path="/boards"
          render={(props) => <BoardsPage filter={query} />}
        />
        <Route
          path="/"
          exact={true}
          render={(props) => <BoardsPage filter={query} />}
        />
        <Route>
          <ErrorPage customMessage="Страница не найдена..." />
        </Route>
      </Switch>
      {/* <Redirect to="/boards" /> */}
    </main>
  );
};

const mapStateToProps = ({ boards }) => ({
  boards,
});

const mapDispatchToProps = (dispatch) => ({
  requestBoards: () => dispatch(requestBoards()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
