import { authUserAction } from "../store/user/actions";
import { loadingUser } from "../store/loading/actions";
import { BASE_URL } from "../consts/config";
import setUser from "../utils/setUser";

export default () => (dispatch) => {
  dispatch(loadingUser(true));
  const prevInfo = JSON.parse(localStorage.getItem("userInfo"));

  if (prevInfo) {
    dispatch(authUserAction(true, prevInfo));
    dispatch(loadingUser(false));
    return;
  }

  fetch(BASE_URL + "/api/user", {
    method: "GET",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
  })
    .then((res) => res.json())
    .then(
      (data) => {
        dispatch(authUserAction(data.authenticated, data.user));
        setUser(data.user);
        dispatch(loadingUser(false));
      },
      (error) => console.log("Error:", error)
    );
};
