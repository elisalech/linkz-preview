import { BASE_URL } from "../consts/config";

export default (query, handler) => {
  fetch(`${BASE_URL}/api/help?url=${query}`, {
    method: "POST",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
  })
    .then((res) => res.json())
    .then(({ error, results }) => {
      const { data } = results;
      handler({ error, data });
    });
};
