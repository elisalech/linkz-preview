export default (author) => {
  return fetch(`/api/boards/author/${author}`, {
    method: "POST",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
  })
    .then((res) => res.json())
    .then(({ author }) => author);
};
