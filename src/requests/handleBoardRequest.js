import { loadingBoards } from "../store/loading/actions";
import { updateBoard, addBoard, removeBoard } from "../store/boards/actions";
import { toggleNewBoard } from "../store/togglers/actions";
import { resetUser } from "../store/user/actions";
import setUser from "../utils/setUser";

export default (id = "", method, params = {}) => (dispatch) => {
  dispatch(loadingBoards(true));
  fetch(`/api/boards/${method}/${id}`, {
    method: "POST",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
    body: JSON.stringify(params),
  })
    .then((res) => res.json())
    .then((data) => {
      const { message } = data;
      // eslint-disable-next-line default-case
      switch (message) {
        case "DELETED":
          dispatch(removeBoard(id));
          break;
        case "UPDATED":
          dispatch(updateBoard(id, data.board));
          break;
        case "FAV":
          setUser(data.user);
          dispatch(resetUser(data.user));
          break;
        case "CREATED":
          dispatch(toggleNewBoard(false));
          setUser(data.user);
          dispatch(resetUser(data.user));
          dispatch(addBoard(data.board));
          break;
      }
      dispatch(loadingBoards(false));
    });
};
