import { loadingLinks } from "../store/loading/actions";
import { setLinks } from "../store/links/actions";

import { BASE_URL } from "../consts/config";
import { toggleError, setErrorMessage } from "../store/errors/actions";

export default (id) => (dispatch) => {
  dispatch(loadingLinks(true));
  fetch(`${BASE_URL}/api/board/${id}`, {
    method: "GET",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
  })
    .then((res) => res.json())
    .then(({ links, message }) => {
      dispatch(loadingLinks(false));
      switch (message) {
        case "OK":
          dispatch(setLinks(links));
          break;
        case "Board not found":
          dispatch(toggleError(true));
          dispatch(setErrorMessage("Доска не найдена"));
          break;
        case "You have no access":
          dispatch(toggleError(true));
          dispatch(setErrorMessage("Кажется, у Вас нет доступа"));
          break;
        default:
          break;
      }
    });
};
