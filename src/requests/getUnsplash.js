import Unsplash, { toJson } from "unsplash-js";
import { loadingLinks } from "../store/loading/actions";

import { ACCESS_KEY, SECRET_KEY } from "../consts/config";

const unsplash = new Unsplash({
  accessKey: ACCESS_KEY,
  secret: SECRET_KEY,
});

export default (query, handleRes) => (dispatch) => {
  dispatch(loadingLinks(true));
  unsplash.search
    .photos(query, 1, 30, { orientation: "landscape" })
    .then(toJson)
    .then(({ results }) => {
      handleRes(
        results.map(({ urls, user }) => ({
          img: urls.regular,
          username: user.username,
          link: user.links.html,
        }))
      );
      dispatch(loadingLinks(false));
    });
};
