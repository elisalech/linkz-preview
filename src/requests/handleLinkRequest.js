import { loadingLinks } from "../store/loading/actions";
import { removeLink, updateLink, addLink } from "../store/links/actions";
import { toggleNewLink } from "../store/togglers/actions";

export default (id, method, params = {}) => (dispatch) => {
  dispatch(loadingLinks(true));
  fetch(`/api/board/${method}/${id}`, {
    method: "POST",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
    body: JSON.stringify(params),
  })
    .then((res) => res.json())
    .then((data) => {
      const { message } = data;

      // eslint-disable-next-line default-case
      switch (message) {
        case "DELETED":
          dispatch(removeLink(id));
          break;
        case "UPDATED":
          dispatch(updateLink(id, data.link));
          break;
        case "CREATED":
          dispatch(toggleNewLink(false));
          dispatch(addLink(data.link));
          break;
      }
      dispatch(loadingLinks(false));
    });
};
