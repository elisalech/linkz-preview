import { loadingBoards } from "../store/loading/actions";
import { setBoards } from "../store/boards/actions";
import { BASE_URL } from "../consts/config";

export default () => (dispatch) => {
  dispatch(loadingBoards(true));
  fetch(BASE_URL + "/api/boards", {
    method: "GET",
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
    },
  })
    .then((res) => res.json())
    .then(({ boards }) => {
      dispatch(setBoards(boards));
      dispatch(loadingBoards(false));
    });
};
