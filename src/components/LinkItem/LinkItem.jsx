import React, { useState } from "react";

import style from "./LinkItem.module.css";
import { CORS_PROXY } from "../../consts/config";
import Control from "../Control/Control";
import { connect } from "react-redux";

import { DelComponent } from "../../UI/icons";
import handleLinkRequest from "../../requests/handleLinkRequest";
import {
  setConfirmationTitle,
  setConfirmHandler,
  toggleConfirmation,
} from "../../store/togglers/actions";

const LinkItem = ({
  link,
  cover,
  title,
  description,
  islogged,
  isAuthor,
  id,
  handleDelete,
}) => {
  const [controls, setControls] = useState(false);
  return (
    <div
      onMouseOver={() => setControls(true)}
      onMouseLeave={() => setControls(false)}
      className={style.link_item}
    >
      <a target="_blank" rel="noopener noreferrer" href={link}>
        <div className={style.link_cover}>
          <img src={CORS_PROXY + cover} alt="" />
        </div>
        <div className={style.link_content}>
          <h2 className={style.link_title}>{title}</h2>
          <div className={style.link_description}>{description}</div>
        </div>
      </a>
      {islogged && isAuthor && controls && (
        <div className={style.controls}>
          <Control onClick={() => handleDelete(id)} content={DelComponent} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = ({ user: { info } }, { author }) => {
  if (!info) return { islogged: info };
  return {
    isAuthor: info._id === author,
    islogged: info,
  };
};
const mapDispatchToProps = (dispatch) => ({
  handleDelete: (id) => {
    dispatch(setConfirmationTitle("Вы действительно хотите удалить ссылку?"));
    const handler = () => dispatch(handleLinkRequest(id, "delete"));
    dispatch(setConfirmHandler(handler));
    dispatch(toggleConfirmation(true));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LinkItem);
