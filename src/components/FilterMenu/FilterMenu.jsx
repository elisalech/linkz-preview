import React from "react";
import { NavLink } from "react-router-dom";

import style from "./FilterMenu.module.css";
import queryList from "../../consts/queryList";

export default ({ isLogged }) => {
  return (
    <div className={style.filter_container}>
      {isLogged ? (
        queryList.map((q, i) => (
          <div className={style.filter_item} key={i}>
            <NavLink
              activeClassName={style.activeItem}
              isActive={(match, location) =>
                q.title === "Все"
                  ? location.pathname + location.search === "/boards"
                  : location.pathname + location.search ===
                    `/boards?query=${q.link}`
              }
              to={q.title === "Все" ? "/boards" : "/boards?query=" + q.link}
            >
              {q.title}
            </NavLink>
          </div>
        ))
      ) : (
        <h2 className={style.header_notlogged}>Публичные доски:</h2>
      )}
    </div>
  );
};
