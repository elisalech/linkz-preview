import React, { useState, useEffect } from "react";

import style from "./BoardSelector.module.css";

export default ({ handleSelect, preBoard, boards }) => {
  const [active, setActive] = useState(null);

  useEffect(() => {
    if (preBoard) setActive(preBoard._id);
  }, [preBoard]);

  const handleClick = (title, _id) => {
    handleSelect({ title, _id });
    setActive(_id);
  };

  return (
    <ul className={style.selector_wrap}>
      {boards.map(({ _id, title }) => (
        <li
          key={_id}
          className={_id === active ? style.active : style.non_active}
          onClick={() => handleClick(title, _id)}
        >
          {title}
        </li>
      ))}
    </ul>
  );
};
