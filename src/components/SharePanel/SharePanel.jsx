import React, { useState, useRef } from "react";

import {
  FacebookShareButton,
  TwitterShareButton,
  VKShareButton,
  FacebookIcon,
  TwitterIcon,
  VKIcon,
} from "react-share";

import style from "./SharePanel.module.css";

import Popup from "../../UI/Popup/Popup";
import { connect } from "react-redux";
import { toggleShare } from "../../store/togglers/actions";
import { BASE_LINK } from "../../consts/config";

const SharePanel = ({ shareUrl, title, image, handleClose }) => {
  const textAreaRef = useRef(null);
  const [copied, setCopied] = useState(false);

  const handleModal = () => {
    setCopied(true);

    setTimeout(() => setCopied(false), 1340);
  };

  const copyToClipboard = (e) => {
    textAreaRef.current.select();
    document.execCommand("copy");
    e.target.focus();
    handleModal();
  };

  return (
    <Popup onClickBackground={handleClose}>
      <div className={style.container}>
        <div onClick={handleClose} className={style.cross}></div>
        <h2 className={style.title}>Публичный доступ</h2>
        <p className={style.description}>
          Контент доски открыт для просмотра по ссылке:
        </p>
        <div className={style.panel__wrap}>
          <div className={style.left}>
            <input
              className={style.textarea}
              ref={textAreaRef}
              value={copied ? "Ccылка скопирована в буфер обмена." : shareUrl}
              onClick={copyToClipboard}
            />

            <button onClick={copyToClipboard} className={style.button}>
              Скопировать
            </button>
          </div>
          <div className={style.social_network__wrap}>
            <FacebookShareButton url={shareUrl} quote={title}>
              <FacebookIcon size={32} round />
            </FacebookShareButton>
            <TwitterShareButton url={shareUrl} title={title}>
              <TwitterIcon size={32} round />
            </TwitterShareButton>
            <VKShareButton title={title} url={shareUrl} image={image}>
              <VKIcon size={32} round />
            </VKShareButton>
          </div>
        </div>
      </div>
    </Popup>
  );
};

const mapStateToProps = ({ togglers: { shareId }, boards }) => {
  const board = boards.find((b) => b._id === shareId);
  const shareUrl = `${BASE_LINK}board/${shareId}`;
  return { image: board.cover, title: board.title, shareUrl };
};
const mapDispatchToProps = (dispatch) => ({
  handleClose: () => dispatch(toggleShare(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SharePanel);
