import React, { useEffect, useState } from "react";

import style from "./BoardHeader.module.css";

import arrow from "../../assets/icons/arrow.svg";
import getLabel from "../../utils/getLabel";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import getAuthor from "../../requests/getAuthor";
import Control from "../Control/Control";

import handleBoardRequest from "../../requests/handleBoardRequest";
import {
  toggleShare,
  setShareAction,
  setConfirmationTitle,
  setConfirmHandler,
  toggleConfirmation,
} from "../../store/togglers/actions";

import {
  DotsComponent,
  FavComponent,
  DelComponent,
  PublicComponent,
} from "../../UI/icons";

const BoardHeader = ({
  title,
  description,
  label,
  len,
  isFav,
  user,
  author,
  id,
  handleFav,
  handleCommon,
  handleDelete,
  toggleShare,
  isAuthor,
}) => {
  const [authorName, setAuthorName] = useState("");
  const [details, setDetails] = useState(false);
  useEffect(() => {
    getAuthor(id).then((data) => {
      if (!data) return;
      setAuthorName(data.name);
    });
  }, [id]);

  return (
    <section className={style.board__header}>
      <div className={style.arrow}>
        <Link to="/boards">
          <img src={arrow} alt="На главную" />
        </Link>
      </div>
      <div className={style.board__content}>
        <div className={style.left}>
          <h1 className={style.board__title}>{title}</h1>
          <p className={style.board__description}>{description}</p>

          <div className={style.board__info}>
            <div className={style.board__label}>{getLabel(label)}</div>
            <span>•</span>
            <div className={style.board__label}>{len || 0}</div>
            {isFav && <div className={style.board__label}>{FavComponent}</div>}
            <span>•</span>
            {user && author === user._id ? "Моя" : authorName}
          </div>
        </div>

        <div className={style.right}>
          {details ? (
            <div
              className={style.controls}
              onMouseLeave={() => setDetails(false)}
            >
              <Control
                active={isFav}
                onClick={() => handleFav(id)}
                content={FavComponent}
              />
              {isAuthor && (
                <>
                  <Control
                    onClick={() => handleCommon(id, label)}
                    active={label === "common"}
                    content={PublicComponent}
                  />
                  <Control
                    onClick={() => handleDelete(id)}
                    content={DelComponent}
                  />
                </>
              )}
            </div>
          ) : (
            <div className={style.controls}>
              {label === "common" && (
                <Control onClick={() => toggleShare(id)} content="Поделиться" />
              )}
              {user && (
                <Control
                  onMouseOver={() => setDetails(true)}
                  content={DotsComponent}
                />
              )}
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = ({ user: { info } }, { id, author }) => {
  if (!info) return { user: info };
  return {
    isFav: info.favorites.includes(id),
    user: info,
    isAuthor: info._id === author,
  };
};

const mapDispatchToProps = (dispatch) => ({
  handleFav: (id) => dispatch(handleBoardRequest(id, "fav")),
  handleCommon: (id, label) => {
    const newLabel = label === "private" ? "публичным" : "личным";
    dispatch(
      setConfirmationTitle(`Вы действительно хотите сделать борд ${newLabel}?`)
    );
    const handler = () => dispatch(handleBoardRequest(id, "common"));
    dispatch(setConfirmHandler(handler));
    dispatch(toggleConfirmation(true));
  },
  handleDelete: (id) => {
    dispatch(setConfirmationTitle("Вы действительно хотите удалить борд?"));
    const handler = () => dispatch(handleBoardRequest(id, "delete"));
    dispatch(setConfirmHandler(handler));
    dispatch(toggleConfirmation(true));
  },
  toggleShare: (id) => {
    dispatch(setShareAction(id));
    dispatch(toggleShare(true));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(BoardHeader);
