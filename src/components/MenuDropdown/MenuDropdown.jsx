import React from "react";
import { connect } from "react-redux";

import style from "./MenuDropdown.module.css";
import { toggleUserAction } from "../../store/togglers/actions";
import logoutHandler from "../../utils/logoutHandler";
import { useHistory } from "react-router-dom";

const MenuDropdown = ({ user }) => {
  const history = useHistory();
  const loginHandler = () => {
    history.push("/auth");
  };
  return (
    <div className={style.menu_dropdown__container}>
      {user && <div className={style.menu_dropdown__username}>{user.name}</div>}
      <ul className={style.menu_dropdown__list}>
        {/* <li>О нас</li> */}
        {user ? (
          <li onClick={logoutHandler}>Выйти</li>
        ) : (
          <li onClick={loginHandler}>Войти</li>
        )}
      </ul>
    </div>
  );
};

const mapStateToProps = ({ user: { info } }) => ({ user: info });
const mapDispatchToProps = (dispatch) => ({
  closeUser: () => dispatch(toggleUserAction(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuDropdown);
