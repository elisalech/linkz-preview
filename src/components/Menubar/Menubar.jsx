import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import _ from "underscore";
import cn from "classnames";

import style from "./Menubar.module.css";
import logo from "../../assets/logo.svg";
import Avatar from "../../UI/Avatar";
import { connect } from "react-redux";
import {
  toggleNewBoard,
  toggleNewLink,
  toggleUserAction,
} from "../../store/togglers/actions";
import useBoardPathname from "../../hooks/useBoardPathname";
import hasScrolled from "../../utils/hasScrolled";
import MenuDropdown from "../MenuDropdown/MenuDropdown";

import NO_IMG from "../../assets/icons/noname.svg";

const Menubar = ({
  openBoardForm,
  id,
  openLinkForm,
  toggleUser,
  showUser,
  userBoards,
}) => {
  const { isBoard, boardId } = useBoardPathname();
  const hasBoard = userBoards && userBoards.includes(boardId);
  const menuEl = useRef(null);
  const [toggleCls, setToggleCls] = useState("nav-down");
  let menuH;
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    menuH = menuEl.current.offsetHeight;
    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  });

  const handleScroll = (e) => {
    _.throttle(hasScrolled(menuH, setToggleCls), 300);
  };

  return (
    <header
      ref={menuEl}
      className={cn(style.menubar_container, style[toggleCls])}
    >
      <div className={style.logo}>
        <Link to="/boards">
          <img src={logo} alt="" />
        </Link>
      </div>
      <div className={style.menubar_right}>
        {id && (
          <div className={style.actions}>
            {!isBoard ? (
              <div className={style.toggler_action} onClick={openBoardForm}>
                Добавить подборку
              </div>
            ) : (
              hasBoard && (
                <div className={style.toggler_action} onClick={openLinkForm}>
                  Добавить ссылку
                </div>
              )
            )}
          </div>
        )}
        <div className={style.menubar_search}></div>
        <div className={style.menubar_user__wrap}>
          <div
            className={style.menubar_avatar}
            onClick={() => toggleUser(!showUser)}
          >
            {id ? (
              <Avatar width="30" avatarId={id} />
            ) : (
              // eslint-disable-next-line jsx-a11y/alt-text
              <img alt="" src={NO_IMG}></img>
            )}
          </div>
          {showUser && <MenuDropdown />}
        </div>
      </div>
    </header>
  );
};
const mapStateToProps = ({ togglers: { user }, user: { info } }) => {
  if (!info)
    return {
      showUser: user,
      id: false,
    };
  return {
    showUser: user,
    id: info._id,
    userBoards: info.boards,
  };
};

const marDispatchToProps = (dispatch) => ({
  openBoardForm: () => dispatch(toggleNewBoard(true)),
  openLinkForm: () => dispatch(toggleNewLink(true)),
  toggleUser: (bool) => dispatch(toggleUserAction(bool)),
});

export default connect(mapStateToProps, marDispatchToProps)(Menubar);
