import React, { useState, useEffect } from "react";
import style from "./LinkForm.module.css";
import { connect } from "react-redux";
import { debounce } from "underscore";

import { toggleNewLink } from "../../store/togglers/actions";
import handleLinkRequest from "../../requests/handleLinkRequest";
import getLinkPreview from "../../requests/getLinkPreview";
import LinkItem from "../LinkItem/LinkItem";
import BoardSelector from "../BoardSelector/BoardSelector";
import Popup from "../../UI/Popup/Popup";
import useBoardPathname from "../../hooks/useBoardPathname";

const LinkForm = ({ submitForm, closeForm, boards }) => {
  const [link, setLink] = useState("");
  const [title, setTitle] = useState("");
  const [cover, setCover] = useState("");
  const [description, setDescription] = useState("");
  const [preview, setPreview] = useState(null);
  const [selector, setSelector] = useState(false);
  const [board, setBoard] = useState({});
  const { boardId } = useBoardPathname();

  useEffect(() => {
    const { title, _id } = boards.find((b) => b._id == boardId);
    if (title) setBoard({ title, _id });
  }, []);

  useEffect(() => {
    if (!preview) {
      setTitle("");
      setDescription("");
      setCover("");
      return;
    }
    const { error, data } = preview;
    if (error) {
      console.log("Error:", preview);
      setTitle("");
      setDescription("");
      setCover("");
      setPreview(null);
      return;
    }
    const { ogDescription, ogTitle, ogImage } = data;

    let url;
    if (!Array.isArray(ogImage)) {
      url = ogImage.url;
    } else if (ogImage.length) {
      url = ogImage[0].url;
    } else url = "/nocover.png";

    setTitle(ogTitle);
    setDescription(ogDescription);
    setCover(url);
  }, [preview]);

  const handleLinkChange = (e) => {
    e.preventDefault();
    setLink(e.target.value);

    debounce(getLinkPreview(e.target.value, setPreview), 150);
  };

  const handleClose = (e) => {
    e.preventDefault();
    closeForm();
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    if (!title) return;
    submitForm({ title, description, cover, link, board: board._id });
  };
  return (
    <Popup onClickBackground={handleClose}>
      <form className={style.form} onSubmit={handleSubmit}>
        <h2 className={style.popup_header}>Сохранение ссылки</h2>
        <div className={style.form_field}>
          <label htmlFor="link">Ссылка</label>
          <textarea
            className={style.form_input}
            name="link"
            id="link"
            onChange={handleLinkChange}
          ></textarea>
        </div>
        <div className={style.form_field}>
          <label htmlFor="board">Борд</label>
          <div
            id="board"
            className={style.form_input}
            onClick={() => setSelector(!selector)}
          >
            <span>{board.title || "Выберите доску"}</span>
            <span
              className={[
                style.arrow,
                selector ? style.arrow_clicked : "",
              ].join(" ")}
            ></span>
          </div>
          {selector && (
            <BoardSelector
              boards={boards}
              preBoard={board}
              handleSelect={setBoard}
            />
          )}
        </div>

        <div className={style.link_preview}>
          {preview && (
            <LinkItem cover={cover} title={title} description={description} />
          )}
        </div>

        <div className={style.board__buttons}>
          <input type="submit" value="Сохранить" onSubmit={handleSubmit} />
          <button onClick={handleClose}>Отменить</button>
        </div>
      </form>
    </Popup>
  );
};
const mapStateToProps = ({
  boards,
  user: {
    info: { _id },
  },
}) => ({
  boards: boards.filter((b) => b.author == _id),
});

export default connect(mapStateToProps, (dispatch) => ({
  closeForm: () => dispatch(toggleNewLink(false)),
  submitForm: (params) =>
    dispatch(handleLinkRequest(params.board, "create", params)),
}))(LinkForm);
