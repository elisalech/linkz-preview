import React, { useState } from "react";
import style from "./BoardForm.module.css";
import { connect } from "react-redux";

import { toggleNewBoard } from "../../store/togglers/actions";
import handleBoardRequest from "../../requests/handleBoardRequest";
import CoverSelector from "../CoverSelector/CoverSelector";

const BoardForm = ({ submitForm, closeForm }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [cover, setCover] = useState("");

  const handleClose = (e) => {
    e.preventDefault();
    closeForm();
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    if (!title) return;
    submitForm({ title, description, cover });
  };
  return (
    <form className={style.board_form__container} onSubmit={handleSubmit}>
      <div className={style.board__content}>
        <input
          className={style.board__title}
          type="text"
          name="title"
          placeholder="Название"
          onChange={(e) => setTitle(e.target.value)}
        />
        <input
          className={style.board__description}
          type="text"
          name="description"
          placeholder="Краткое описание борда…"
          onChange={(e) => setDescription(e.target.value)}
        />
      </div>
      <CoverSelector handleSelect={setCover} />
      <div className={style.board__buttons}>
        <input type="submit" value="Сохранить" onSubmit={handleSubmit} />
        <button onClick={handleClose}>Отменить</button>
      </div>
    </form>
  );
};

export default connect(null, (dispatch) => ({
  closeForm: () => dispatch(toggleNewBoard(false)),
  submitForm: (params) => dispatch(handleBoardRequest("", "create", params)),
}))(BoardForm);
