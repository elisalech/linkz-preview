import React from "react";

import style from "./ConfirmModal.module.css";
import Popup from "../../UI/Popup/Popup";
import { connect } from "react-redux";
import { toggleConfirmation } from "../../store/togglers/actions";

const ConfirmModal = ({ onClose, title, handleSubmit }) => {
  const handleClickSubmit = (e) => {
    e.preventDefault();
    handleSubmit();
    onClose();
  };
  return (
    <Popup onClickBackground={onClose}>
      <div className={style.container}>
        <h3 className={style.title}>{title}</h3>
        <div className={style.buttons}>
          <input
            type="submit"
            value="Подтвердить"
            onClick={handleClickSubmit}
          />
          <button onClick={onClose}>Отменить</button>
        </div>
      </div>
    </Popup>
  );
};
const mapStateToProps = ({ togglers: { confirmTitle, confirmHandler } }) => ({
  title: confirmTitle,
  handleSubmit: confirmHandler,
});

const mapDispatchToProps = (dispatch) => ({
  onClose: () => dispatch(toggleConfirmation(false)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmModal);
