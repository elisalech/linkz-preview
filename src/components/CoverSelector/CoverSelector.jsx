import React, { useState } from "react";
import { connect } from "react-redux";

import style from "./CoverSelector.module.css";
import getUnsplash from "../../requests/getUnsplash";
import debounce from "../../utils/debounce";
import Loader from "../../UI/Loader/Loader";

const CoverSelector = ({ handleSelect, loading, getUnsplash }) => {
  const [results, setResults] = useState(null);
  const [active, setActive] = useState(null);

  const handleChange = ({ target: { value } }) => {
    debounce(() => getUnsplash(value, setResults), 300);
  };
  const handleClick = (image) => {
    setActive(image);
    handleSelect(image);
  };

  const renderImages = (res) => (
    <div className={style.results_container}>
      {res.map(({ img, username, link }) => (
        <div className={style.item_wrap}>
          <img
            src={img}
            alt=""
            key={img}
            onClick={() => handleClick(img)}
            className={[
              style.result_item,
              img === active ? style.active : "",
            ].join(" ")}
          />
          <span>
            by{" "}
            <a href={link} rel="noopener noreferrer" target="_blank">
              {username}
            </a>
          </span>
        </div>
      ))}
    </div>
  );
  return (
    <div className={style.selector_wrap}>
      <input
        type="text"
        onChange={handleChange}
        placeholder="Поиск обложки (eng)"
      />
      {loading ? (
        <Loader title="Картинки грузятся..." />
      ) : (
        results &&
        results.length > 0 && (
          <>
            {renderImages(results)}
            <div className={style.gradient_wrap}></div>
          </>
        )
      )}
    </div>
  );
};

const mapStateToProps = ({ loading: { links } }) => ({ loading: links });
const mapDispatchToProps = (dispatch) => ({
  getUnsplash: (query, func) => dispatch(getUnsplash(query, func)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CoverSelector);
