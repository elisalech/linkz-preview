import React from "react";

import style from "./Control.module.css";

export default ({ content, onClick, onMouseOver, active }) => {
  return (
    <div
      className={[style.control_item, active ? style.active : ""].join(" ")}
      onMouseOver={onMouseOver}
      onClick={onClick}
    >
      {content}
    </div>
  );
};
