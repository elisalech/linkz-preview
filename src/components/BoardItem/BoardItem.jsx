import React, { useState } from "react";

import style from "./BoardItem.module.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Control from "../Control/Control";

import getRandomColor from "../../utils/getRandomColor";
import getLabel from "../../utils/getLabel";

import handleBoardRequest from "../../requests/handleBoardRequest";
import {
  DotsComponent,
  FavComponent,
  DelComponent,
  PublicComponent,
} from "../../UI/icons";
import {
  toggleShare,
  setShareAction,
  setConfirmationTitle,
  setConfirmHandler,
  toggleConfirmation,
} from "../../store/togglers/actions";

const BoardItem = ({
  label,
  title,
  description,
  cover,
  id,
  len,
  handleFav,
  handleCommon,
  isFav,
  handleDelete,
  isAuthor,
  islogged,
  toggleShare,
}) => {
  const [details, setDetails] = useState(false);
  const [controls, setControls] = useState(false);
  return (
    <article
      onMouseOver={() => setControls(true)}
      onMouseLeave={() => setControls(false)}
      className={style.board}
    >
      <Link to={`/board/${id}`}>
        <div
          className={style.board__cover}
          style={{
            background: cover
              ? `url(${cover}) no-repeat center`
              : getRandomColor(),
          }}
        ></div>

        <div className={style.board__content}>
          <h1 className={style.board__title}>{title}</h1>
          <p className={style.board__description}>{description}</p>

          <div className={style.board__info}>
            <div className={style.board__label}>{getLabel(label)}</div>
            <span>•</span>
            <div className={style.board__label}>{len || 0}</div>
            {isFav && FavComponent}
          </div>
        </div>
      </Link>
      {controls &&
        (details ? (
          <div
            className={style.controls}
            onMouseLeave={() => setDetails(false)}
          >
            <Control
              active={isFav}
              onClick={() => handleFav(id)}
              content={FavComponent}
            />
            {isAuthor && (
              <>
                <Control
                  onClick={() => handleCommon(id, label)}
                  active={label === "common"}
                  content={PublicComponent}
                />
                <Control
                  onClick={() => handleDelete(id)}
                  content={DelComponent}
                />
              </>
            )}
          </div>
        ) : (
          <div className={style.controls}>
            {label === "common" && (
              <Control onClick={() => toggleShare(id)} content="Поделиться" />
            )}
            {islogged && (
              <Control
                onMouseOver={() => setDetails(true)}
                content={DotsComponent}
              />
            )}
          </div>
        ))}
    </article>
  );
};

const mapStateToProps = ({ user: { info } }, { id, author }) => {
  if (!info) return { islogged: info };
  return {
    isFav: info.favorites.includes(id),
    isAuthor: info._id === author,
    islogged: info,
  };
};

const mapDispatchToProps = (dispatch) => ({
  handleFav: (id) => dispatch(handleBoardRequest(id, "fav")),
  handleCommon: (id, label) => {
    const newLabel = label === "private" ? "публичным" : "личным";
    dispatch(setConfirmationTitle(`Вы действительно хотите борд ${newLabel}?`));
    const handler = () => dispatch(handleBoardRequest(id, "common"));
    dispatch(setConfirmHandler(handler));
    dispatch(toggleConfirmation(true));
  },
  handleDelete: (id) => {
    dispatch(setConfirmationTitle("Вы действительно хотите удалить борд?"));
    const handler = () => dispatch(handleBoardRequest(id, "delete"));
    dispatch(setConfirmHandler(handler));
    dispatch(toggleConfirmation(true));
  },
  toggleShare: (id) => {
    dispatch(setShareAction(id));
    dispatch(toggleShare(true));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(BoardItem);
