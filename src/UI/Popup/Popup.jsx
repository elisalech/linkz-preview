import React from "react";

import style from "./Popup.module.css";

export default ({ children, dark, onClickBackground }) => {
  return (
    <div
      className={[style.popup__background, dark ? style.dark : ""].join(" ")}
      onClick={onClickBackground}
    >
      <div
        onClick={(e) => e.stopPropagation()}
        className={style.popup_container}
      >
        {children}
      </div>
    </div>
  );
};
