import React from "react";

import style from "./Loader.module.css";

import spinner from "../../assets/Spinner.svg";

export default ({ title }) => {
  return (
    <div className={style.wrap}>
      <img src={spinner} alt="" />
      <h3 className={style.title}>{title}</h3>
    </div>
  );
};
