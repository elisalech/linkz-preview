import WithImg from "./WithImg";

import dots from "../assets/icons/threedot.svg";
import fav from "../assets/icons/action-favourite.svg";
import edit from "../assets/icons/action-edit.svg";
import del from "../assets/icons/action-delete.svg";
import pub from "../assets/icons/action-collaborative.svg";

export const DotsComponent = WithImg(dots);
export const FavComponent = WithImg(fav);
export const EditComponent = WithImg(edit);
export const DelComponent = WithImg(del);
export const PublicComponent = WithImg(pub);
