import React, { useEffect } from "react";
import requestAuth from "./requests/requestAuth";
import { connect } from "react-redux";

import "./App.css";
import routes from "./routes";
import Loader from "./UI/Loader/Loader";

const App = ({ handleAuth, authenticated, loading }) => {
  useEffect(() => {
    handleAuth();
  }, [handleAuth]);

  if (loading) {
    return <Loader title="Загружаются данные о пользователе..." />;
  }

  return routes(authenticated);
};

const mapStateToProps = ({ loading: { user }, user: { authenticated } }) => ({
  loading: user,
  authenticated,
});

const mapDispatchToProps = (dispatch) => ({
  handleAuth: () => dispatch(requestAuth()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
