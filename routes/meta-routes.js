const path = require("path");
const fs = require("fs");
const Board = require("../models/Board");

const PATH_TO_INDEX = path.resolve(__dirname, "../", "build", "index.html");

const defaultMeta = (req, res) => {
  fs.readFile(PATH_TO_INDEX, "utf8", (err, data) => {
    if (err) {
      return console.log(err);
    }
    res.send(data);
  });
};

const boardMeta = async (req, res) => {
  const id = req.params.bid;
  const board = await Board.findById(id);

  if (!board) {
    defaultMeta(req, res);
    return;
  }

  const { title, cover, description } = board;

  fs.readFile(PATH_TO_INDEX, "utf8", (err, data) => {
    if (err) {
      return console.log(err);
    }

    data = data.replace(/content="Linkz"/g, `content="${title}"`);
    data = data.replace(
      /content="Create and share your boards with links"/g,
      `content="${description}"`
    );
    data = data.replace(/content="\/logo512\.png"/g, `content="${cover}"`);
    res.send(data);
  });
};

module.exports = { boardMeta, defaultMeta };
