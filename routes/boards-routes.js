const express = require("express");
const { check } = require("express-validator");
const {
  createBoard,
  deleteBoard,
  updateLabel,
  getBoards,
  favBoard,
  getBoardAuthor,
} = require("../controllers/boards-controllers");

const router = express.Router();

router.post("/create/", createBoard);
router.post("/delete/:bid", deleteBoard);
router.post("/fav/:bid", favBoard);
router.post("/common/:bid", updateLabel);
router.post("/author/:bid", getBoardAuthor);

router.get("/", getBoards);

module.exports = router;
