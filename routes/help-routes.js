const { Router } = require("express");
const { getLinkPreview } = require("link-preview-js");
const ogs = require("open-graph-scraper");
const CORS_PROXY = "https://cors-anywhere.herokuapp.com/";

const router = Router();

router.post("/", async (req, res) => {
  const { url } = req.query;

  ogs({ url }, (error, results) => {
    res.json({ error, results });
  });
});

module.exports = router;
