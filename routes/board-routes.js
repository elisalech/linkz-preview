const express = require("express");
const { check } = require("express-validator");
const {
  createLink,
  deleteLink,
  getLinks,
} = require("../controllers/board-controllers");

const router = express.Router();

router.post("/create/:bid", createLink);
router.post("/delete/:lid", deleteLink);

router.get("/:bid", getLinks);

module.exports = router;
