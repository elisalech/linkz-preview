const express = require("express");

const { getUserAll } = require("../controllers/user-controllers.js");
const User = require("../models/User");

const router = express.Router();

const authCheck = (req, res, next) => {
  if (!req.user) {
    res.status(401).json({
      authenticated: false,
      message: "user has not been authenticated",
      user: null,
    });
  } else {
    next();
  }
};

router.get("/", authCheck, async (req, res) => {
  const user = await User.findById(req.user.id);
  res.status(200).json({
    authenticated: true,
    message: "user successfully authenticated",
    user,
  });
});

module.exports = router;
