const passportSetup = require("./config/passport-setup");
const express = require("express");
const config = require("config");
const path = require("path");
const cors = require("cors");
const mongoose = require("mongoose");
const passport = require("passport");
const cookieSession = require("cookie-session");
const cookieParser = require("cookie-parser");

const authRoutes = require("./routes/auth-routes.js");
const userRoutes = require("./routes/user-routes.js");
const boardRoutes = require("./routes/board-routes.js");
const boardsRoutes = require("./routes/boards-routes.js");
const helpRoutes = require("./routes/help-routes.js");
const { boardMeta, defaultMeta } = require("./routes/meta-routes.js");

const app = express();

app.use(
  cors({
    origin: "http://localhost:3000",
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
  })
);

app.use(
  cookieSession({
    name: "linkz-session",
    maxAge: 24 * 60 * 60 * 1000,
    keys: [config.get("cookieKey")],
  })
);

app.use(cookieParser());

app.use(express.json({ extended: true }));

app.use(passport.initialize());
app.use(passport.session());

app.use("/api/help/", helpRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/user", userRoutes);
app.use("/api/boards", boardsRoutes);
app.use("/api/board", boardRoutes);

if (process.env.NODE_ENV === "production") {
  app.use("/", express.static(path.join(__dirname, "build")));

  app.get("/board/:bid", boardMeta);

  app.get("*", defaultMeta);
}

const PORT = process.env.PORT || 5000;

async function start() {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    app.listen(PORT, () =>
      console.log(`Ssylki started on ${PORT}. Here we're go...`)
    );
  } catch (e) {
    console.log("Server Error", e.message);
    process.exit(1);
  }
}

start();
