// const { validationResult } = require("express-validator");
const mongoose = require("mongoose");

const Link = require("../models/Link");
const Board = require("../models/Board");

const createLink = async (req, res, next) => {
  const { bid } = req.params;
  const { title, description, cover, link } = req.body;

  const newLink = new Link({
    title,
    cover,
    description,
    link,
    board: bid,
  });

  const board = await Board.findById(bid);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await newLink.save({ session: sess });
    board.links.push(newLink);
    await board.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    console.log("ERROR", err);
  }
  res.status(201).json({ link: newLink, message: "CREATED" });
};

const deleteLink = async (req, res, next) => {
  const author = req.user.id;
  const linkId = req.params.lid;

  let link;
  try {
    link = await Link.findById(linkId).populate("board");
  } catch (err) {
    res.status(500).json({ message: "Something went wrong with link..." });
  }

  if (!link) {
    res.status(404).json({ message: "Link not found" });
  }
  const board = link.board;
  if (board.author._id != author) {
    res
      .status(404)
      .json({ message: "You have no rights to delete this board" });
    return;
  }

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await link.remove({ session: sess });
    link.board.links.pull(link);
    // board.author.favorites.pull(board);
    await link.board.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    res.status(500).json({ message: "Something went wrong with link..." });
  }

  res.status(200).json({ message: "DELETED" });
};

const getLinks = async (req, res, next) => {
  const { bid } = req.params;
  let board;

  try {
    board = await Board.findById(bid);
  } catch (err) {
    console.log("---BOARD ERROR---", err);
    return res.status(404).json({ message: "Board not found" });
  }
  if (!board) return res.status(404).json({ message: "Board not found" });

  if (req.user) {
    const author = req.user.id;

    if (board.author != author && board.label !== "common") {
      return res.status(401).json({ message: "You have no access" });
    }
  } else {
    if (board.label !== "common") {
      return res.status(401).json({ message: "You have no access" });
    }
  }

  const { links } = await Board.findById(bid).populate("links");

  return res.json({ links: links.reverse(), message: "OK" });
};

module.exports = {
  createLink,
  deleteLink,
  getLinks,
};
