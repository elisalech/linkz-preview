// const { validationResult } = require("express-validator");
const mongoose = require("mongoose");

const Board = require("../models/Board");
const User = require("../models/User");

const getBoards = async (req, res, next) => {
  const commonB = await Board.find({ label: "common" });
  if (!req.user) {
    res.json({
      boards: commonB.reverse(),
    });
    return;
  }

  const author = req.user.id;

  const privateB = await Board.find({ author: author });

  const filteredCommon = commonB.filter((b) => b.author != author);

  const boards = [...privateB, ...filteredCommon];

  res.json({
    boards: boards.reverse(),
  });
};

const createBoard = async (req, res, next) => {
  const author = req.user.id;
  const { title, description, cover } = req.body;

  const newBoard = new Board({
    title,
    description,
    author,
    cover,
    label: "private",
  });

  const user = await User.findById(author);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await newBoard.save({ session: sess });
    user.boards.push(newBoard);
    await user.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    console.log("ERROR", err);
  }

  res.status(201).json({ board: newBoard, user, message: "CREATED" });
};

const updateLabel = async (req, res, next) => {
  const author = req.user.id;
  const boardId = req.params.bid;

  const board = await Board.findOne({ _id: boardId, author });

  if (!board) res.status(404).json({ message: "Board not found" });

  board.label === "common"
    ? (board.label = "private")
    : (board.label = "common");

  await board.save();

  res.status(201).json({ board, message: "UPDATED" });
};

const updateBoard = async (req, res, next) => {
  const author = req.user.id;
  const boardId = req.params.bid;

  const { title, description, cover } = req.body;

  const board = await Board.findOne({ _id: boardId, author });

  if (!board) res.status(404).json({ message: "Board not found" });

  board.title = title;
  board.description = description;
  board.cover = cover;

  await board.save();

  res.status(201).json({ board, message: "UPDATED" });
};

const favBoard = async (req, res, next) => {
  const author = req.user.id;
  const boardId = req.params.bid;

  const user = await User.findById(author);
  if (!user) res.status(404).json({ message: "User not found" });
  const board = await Board.findById(boardId);
  if (!board) res.status(404).json({ message: "Board not found" });

  if (user.favorites.includes(boardId)) {
    user.favorites = user.favorites.filter((b) => b != boardId);
  } else {
    await user.favorites.push(boardId);
  }
  await user.save();

  res.json({ message: "FAV", user });
};

const deleteBoard = async (req, res, next) => {
  const author = req.user.id;
  const boardId = req.params.bid;

  let board;
  try {
    board = await Board.findById(boardId).populate("author");
  } catch (err) {
    res.status(500).json({ message: "Something went wrong with board..." });
  }

  if (!board) {
    res.status(404).json({ message: "Board not found" });
  }

  if (board.author._id != author) {
    console.log("board.author.toString()", board.author.toString());
    console.log("author", author);
    res
      .status(404)
      .json({ message: "You have no rights to delete this board" });
    return;
  }

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await board.remove({ session: sess });
    board.author.boards.pull(board);
    board.author.favorites.pull(board);
    await board.author.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    res.status(500).json({ message: "Something went wrong with board..." });
  }

  res.status(200).json({ message: "DELETED" });
};

const getBoardAuthor = async (req, res, next) => {
  const boardId = req.params.bid;

  let board;
  try {
    board = await Board.findById(boardId).populate("author");
  } catch (err) {
    res.status(500).json({ message: "Something went wrong with board..." });
  }

  if (!board) {
    res.status(404).json({ message: "Board not found" });
  }
  const { author } = board;
  res.status(202).json({ message: "Author found", author });
};

module.exports = {
  createBoard,
  deleteBoard,
  updateBoard,
  updateLabel,
  getBoards,
  favBoard,
  getBoardAuthor,
};
