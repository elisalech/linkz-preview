const User = require("../models/User");

const handleUserAuth = async ({ id, displayName, emails, provider }, done) => {
  const current = await User.findOne({ providerInfo: emails[0].value });
  if (current) {
    done(null, current);
  } else {
    const newUser = await new User({
      name: displayName,
      providerInfo: emails[0].value,
      provider,
    });
    await newUser.save();

    done(null, newUser);
  }
};

const handleDeserialize = async (id) => {
  const user = await User.findById(id);
  return user;
};

module.exports = { handleUserAuth, handleDeserialize };
